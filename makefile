# Create production build of frontend
build-frontend:
	cd ./frontend && npm run build

# Start frontend dev server from root directory
start-frontend:
	cd ./frontend && npm start