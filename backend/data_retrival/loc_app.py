import requests
import json
import time

MAX_RETRIES = 3
DELAY_BETWEEN_RETRIES = 2

def make_api_request(url, params):
    for attempt in range(MAX_RETRIES):
        response = requests.get(url, params=params)
        data = response.json()

        if data.get('status') == 'OK':
            return data

        print(f"Error in API response: {data.get('status')}. Retrying...")

        time.sleep(DELAY_BETWEEN_RETRIES)

    print(f"Max retries reached. Unable to get a successful response.")
    return None

def get_coordinates(api_key, zip_code):
    geocoding_url = 'https://maps.googleapis.com/maps/api/geocode/json'

    params = {
        'address': zip_code,
        'key': api_key,
    }

    data = make_api_request(geocoding_url, params)

    # Extract coordinates from the response
    location = data.get('results', [{}])[0].get('geometry', {}).get('location', {})
    lat, lng = location.get('lat', None), location.get('lng', None)

    return lat, lng

def get_nearby_places(api_key, location, type, radius=5000):
    places_url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
    
    params = {
        'location': f'{location[0]},{location[1]}',
        'radius': radius,
        'key': api_key,
        'types': type
    }

    all_results = []
    page_cnt = 0
    while True:
        # Make the request
        page_cnt += 1
        print("Getting page ", page_cnt)
        data = make_api_request(places_url, params)

        # Check if the request was successful
        if data and data.get('status') == 'OK':
            # Add current page results to the list
            all_results.extend(data.get('results'))

            # Check if there are more pages
            next_page_token = data.get('next_page_token')
            if not next_page_token:
                break
            if next_page_token:
                break

            time.sleep(DELAY_BETWEEN_RETRIES)
            # Include the next page token in the params for the next request
            params['pagetoken'] = next_page_token
        else:
            break

    return all_results

def get_place_details(api_key, place_id):
    base_url = 'https://maps.googleapis.com/maps/api/place/details/json'
    
    params = {
        'place_id': place_id,
        'key': api_key,
    }

    data = make_api_request(base_url, params)

    if data:
        data = data.get('result')
        wheelchair = data.get("wheelchair_accessible_entrance", False)
        if wheelchair : 
            # Extract city and zip code from address_components
            city_name = ""
            zip_code = ""
            for component in data.get("address_components", []):
                types = component.get("types", [])
                long_name = component.get("long_name", "")
                
                if "locality" in types and "political" in types:
                    city_name = long_name
                elif "postal_code" in types:
                    zip_code = long_name

            # Extract the first photo
            first_photo = data.get("photos", [])[0] if data.get("photos") else {}
            first_photo_info = {
                "height": first_photo.get("height", 0),
                "width": first_photo.get("width", 0),
                "photo_reference": first_photo.get("photo_reference", "")
            }

            # Extract the periods array for opening hours
            opening_hours_periods = data.get("opening_hours", {}).get("periods", [])

            # Extract only the necessary information for reviews
            reviews = [
                {
                    "author_name": review.get("author_name", ""),
                    "text": review.get("text", ""),
                    "rating": review.get("rating", 0),
                }
                for review in data.get("reviews", [])[:3]
            ]
            # Filter the information you need from the response
            filtered_info = {
                "place_id": data.get("place_id", ""),
                "formatted_address": data.get("formatted_address", ""),
                "name": data.get("name", ""),
                "rating": data.get("rating", 0.0),
                "types": data.get("types", []),
                "opening_hours_periods": opening_hours_periods,
                "reviews": reviews,
                "wheelchair_accessible": wheelchair,
                "city": city_name,
                "zip": zip_code,
                "first_photo": first_photo_info,
            }
            return filtered_info
    return None

def main():
    api_key = ''

    with open('zip_info.json', 'r') as json_file:
        zip_info = json.load(json_file)

    loc_info = []
    place_cnt = 0
    zip_cnt = 0
    for zip_code, coordinates_list in zip_info.items():
        # Assuming only one set of coordinates for simplicity
        zip_cnt += 1
        print("Getting data for: ", zip_cnt)
        coordinates = (float(coordinates_list[0]['latitude']), float(coordinates_list[0]['longitude']))

        # Get nearby places for the ZIP code
        nearby_places = []
        types = ['restaurant', 'tourist_attraction', 'store', 'doctor', 'lodging']
        for place_type in types:
            nearby_places.extend(get_nearby_places(api_key, coordinates, place_type, radius=10000))

        unique_places = set()
        # Get place details for each place within 5KM
        for place in nearby_places:
            place_id = place.get('place_id')
            # Check if the place ID is unique
            if place_id not in unique_places:
                cur_place = get_place_details(api_key, place_id)
                if cur_place:
                    loc_info.append(cur_place)
                    place_cnt += 1
                    # Add the place ID to the set
                    unique_places.add(place_id)

    # Save the loc_info to a new JSON file
    with open('loc_info.json', 'w') as json_file:
        json.dump(loc_info, json_file, indent=2)

    print("Place details have been saved to loc_info.json.")
    print("Number of Zip Codes: ", zip_cnt)
    print("Number of Places: ", place_cnt)

if __name__ == "__main__":
    main()
