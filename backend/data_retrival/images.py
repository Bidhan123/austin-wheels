import requests
import json
import time

def search_images(api_key, query, max_retries=3):
    search_url = 'https://www.googleapis.com/customsearch/v1'
    
    params = {
        'key': api_key,
        'q': query,
        'searchType': 'image',
        'cx': '2313f99887962469a',
        'num': 1  # Limit to one result for simplicity
    }

    retries = 0
    while retries < max_retries:
        response = requests.get(search_url, params=params)
        data = response.json()

        if 'items' in data:
            return data['items']
        else:
            print(f"Error in API response")
            retries += 1
            print(f"Retrying... ({retries}/{max_retries})")
            time.sleep(1)  # Add a short delay before retrying

    print(f"Failed after {max_retries} retries. No images found for {query}")
    return None

def main():
    api_key = 'AIzaSyCwxfEimbWL5mRv6ega-Fh36uQJ9b-o14E'

    count = 0
    fail_count = 0

    with open('loc_info.json', 'r') as json_file:
        loc_info = json.load(json_file)
    
    for entry in loc_info:
        location_name = entry.get('name', '')
        query = f'{location_name}'

        # Perform image search
        images = search_images(api_key, query)

        if images:
            # Update the photo_reference with the image URL
            entry['first_photo']['photo_reference'] = images[0]['link']
            #entries[0]['photo_reference_1'] = images[0]['link']
            #entries[0]['photo_reference_2'] = images[1]['link']
            print(f"{location_name}", count)
            count += 1
        else:
            print(f"No images found for {query}")
            fail_count += 1
        
        time.sleep(.5)

    # Save the updated loc_info to a new JSON file
    with open('loc_info_updated.json', 'w') as json_file:
        json.dump(loc_info, json_file, indent=2)
    
    print("Success: ", count)
    print("Fails: ", fail_count)

if __name__ == "__main__":
    main()
