from models import application, db, Zip, Location, Transport
import json

def populate_database():
    populate_zips()
    populate_locations()
    populate_transport()

def populate_zips():
    with open('zip_info_unique.json') as f:
        zip_data = json.load(f)

    for zip_code, details in zip_data.items():
        zip_obj = Zip(**details[0])
        db.session.add(zip_obj)

    db.session.commit()

def populate_locations():
    with open('loc_info.json') as f:
        location_data = json.load(f)

    for location_info in location_data:
        location_obj = Location(**location_info)
        db.session.add(location_obj)

    db.session.commit()

def populate_transport():
    with open('stop_info.json') as f:
        transport_data = json.load(f)

    for transport_info in transport_data:
        transport_obj = Transport(**transport_info)
        db.session.add(transport_obj)

    db.session.commit()

if __name__ == "__main__":
    with application.app_context():
        db.drop_all()
        db.create_all()
        populate_database()
