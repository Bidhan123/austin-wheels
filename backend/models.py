from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# Configure app
application = Flask(__name__)
CORS(application)
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
application.config["SQLALCHEMY_DATABASE_URI"] =  'mysql://admin:austinwheels@austin-wheels-db.caynscbbvob8.us-east-1.rds.amazonaws.com:3306/austin_wheels'
try:
    db = SQLAlchemy(application)
except Exception as e:
    print(f"Error connecting to the database: {e}")

class Zip(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    postal_code = db.Column(db.String(10))
    country_code = db.Column(db.String(10))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    city = db.Column(db.Text)  
    state = db.Column(db.Text)  
    city_en = db.Column(db.Text)  
    state_en = db.Column(db.Text) 
    state_code = db.Column(db.String(10))
    province = db.Column(db.Text)  
    province_code = db.Column(db.String(10))
    location_count = db.Column(db.Integer)
    stop_count = db.Column(db.Integer)
    photo_reference_1 = db.Column(db.Text) 
    photo_reference_2 = db.Column(db.Text)  

class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    place_id = db.Column(db.String(255))
    formatted_address = db.Column(db.Text)
    name = db.Column(db.Text)
    rating = db.Column(db.Float)
    types = db.Column(db.JSON)  
    opening_hours_periods = db.Column(db.JSON)  
    reviews = db.Column(db.JSON) 
    wheelchair_accessible = db.Column(db.Boolean)
    city = db.Column(db.Text)
    zip = db.Column(db.String(10))
    first_photo = db.Column(db.JSON)

class Transport(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    place_id = db.Column(db.String(255))
    formatted_address = db.Column(db.Text)
    name = db.Column(db.Text)
    rating = db.Column(db.Float)
    types = db.Column(db.JSON) 
    opening_hours_periods = db.Column(db.JSON) 
    reviews = db.Column(db.JSON)  
    wheelchair_accessible = db.Column(db.Boolean)
    city = db.Column(db.Text)
    zip = db.Column(db.String(10))
    lat = db.Column(db.Float)
    long = db.Column(db.Float)
    first_photo = db.Column(db.JSON)


