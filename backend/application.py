from flask import Flask, jsonify, request, abort
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from sqlalchemy.sql import text
import json

# Configure app
application = Flask(__name__)
CORS(application)
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
application.config["SQLALCHEMY_DATABASE_URI"] =  'mysql://admin:austinwheels@austin-wheels-db.caynscbbvob8.us-east-1.rds.amazonaws.com:3306/austin_wheels'
db = SQLAlchemy(application)

ma = Marshmallow()
ma.init_app(application)

class Zip(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    postal_code = db.Column(db.String(10))
    country_code = db.Column(db.String(10))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    city = db.Column(db.Text)  
    state = db.Column(db.Text)  
    city_en = db.Column(db.Text)  
    state_en = db.Column(db.Text) 
    state_code = db.Column(db.String(10))
    province = db.Column(db.Text)  
    province_code = db.Column(db.String(10))
    location_count = db.Column(db.Integer)
    stop_count = db.Column(db.Integer)
    photo_reference_1 = db.Column(db.Text) 
    photo_reference_2 = db.Column(db.Text)  

class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    place_id = db.Column(db.String(255))
    formatted_address = db.Column(db.Text)
    name = db.Column(db.Text)
    rating = db.Column(db.Float)
    types = db.Column(db.JSON)  
    opening_hours_periods = db.Column(db.JSON)  
    reviews = db.Column(db.JSON) 
    wheelchair_accessible = db.Column(db.Boolean)
    city = db.Column(db.Text)
    zip = db.Column(db.String(10))
    first_photo = db.Column(db.JSON)

class Transport(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    place_id = db.Column(db.String(255))
    formatted_address = db.Column(db.Text)
    name = db.Column(db.Text)
    rating = db.Column(db.Float)
    types = db.Column(db.JSON) 
    opening_hours_periods = db.Column(db.JSON) 
    reviews = db.Column(db.JSON)  
    wheelchair_accessible = db.Column(db.Boolean)
    city = db.Column(db.Text)
    zip = db.Column(db.String(10))
    lat = db.Column(db.Float)
    long = db.Column(db.Float)
    first_photo = db.Column(db.JSON)

class ZipSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Zip


class LocationSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Location


class TransportSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Transport

zip_schema = ZipSchema()
location_schema = LocationSchema()
transport_schema = TransportSchema()

@application.route("/")
def home():
    return """
    <h1>Austin Wheels API</h1>
    <h2>Endpoints:</h2>
    <ul>
        <li>
            <h3>Zip Codes</h3>
            <h4> Total: 60 </h4>
            <ul>
                <li>
                    <code>GET /zipcodes</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort by city, county, latitude, longitude, location_count, stop_count, zip</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /zipcodes/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Locations</h3>
            <h4> Total: 3400 </h4>
            <ul>
                <li>
                    <code>GET /locations</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort by name, rating, city, zip</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /locations/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Transportation</h3>
            <h4> Total: 592 </h4>
            <ul>
                <li>
                    <code>GET /transport</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort by name, rating, city, zip</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /transport/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
    </ul>
    """

@application.route("/zipcodes", methods=["GET"])
def zips():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    sort_map = {
        "city": Zip.city.asc(),
        "county": Zip.province.asc(),
        "location_count": Zip.location_count.desc(),
        "stop_count": Zip.stop_count.desc(),
        "latitude": Zip.latitude.desc(),
        "longitude": Zip.longitude.desc(),
        "zip": Zip.postal_code.asc()
    }

    if sort :
        order_by_clause = sort_map[sort]

    if search:
        if sort:
            query = db.session.query(Zip).filter(
                or_(
                    Zip.city.ilike(f"%{search}%"),
                    Zip.province.ilike(f"%{search}%"),
                    Zip.postal_code.ilike(f"%{search}%"),
                    Zip.state.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            query = db.session.query(Zip).filter(
                or_(
                    Zip.city.ilike(f"%{search}%"),
                    Zip.province.ilike(f"%{search}%"),
                    Zip.postal_code.ilike(f"%{search}%"),
                    Zip.state.ilike(f"%{search}%")
                )
            )
    else:
        if sort:
            query = db.session.query(Zip).order_by(order_by_clause)
        else:
            query = db.session.query(Zip)

    # Serialize the data using the zip_schema
    result = zip_schema.dump(query, many=True)

    # Return the JSON response
    return jsonify(result)

@application.route("/zipcodes/<int:id>")
def get_zip(id):
    # Fetch a specific zip code by ID from the database
    zipcode = Zip.query.get(id)

    # Check if the zip code exists
    if not zipcode:
        abort(404, description="Zip code not found")

    # Serialize the data using the zip_schema
    result = zip_schema.dump(zipcode)
    
    # Return the JSON response
    return jsonify(result)

@application.route("/locations", methods=["GET"])
def locations():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    sort_map = {
        "name": Location.name.asc(),
        "rating": Location.rating.desc(),
        "city": Location.city.asc(),
        "zip": Location.zip.asc(),
    }

    if sort :
        order_by_clause = sort_map[sort]

    if search:
        if sort:
            query = db.session.query(Location).filter(
                or_(
                    Location.city.ilike(f"%{search}%"),
                    Location.zip.ilike(f"%{search}%"),
                    Location.name.ilike(f"%{search}%"),
                    Location.types.ilike(f"%{search}%"),
                    Location.reviews.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            query = db.session.query(Location).filter(
                or_(
                    Location.city.ilike(f"%{search}%"),
                    Location.zip.ilike(f"%{search}%"),
                    Location.name.ilike(f"%{search}%"),
                    Location.types.ilike(f"%{search}%"),
                    Location.reviews.ilike(f"%{search}%")
                )
            )
    else:
        if sort:
            query = db.session.query(Location).order_by(order_by_clause)
        else:
            query = db.session.query(Location)

    # Serialize the data using the location_schema
    result = location_schema.dump(query, many=True)

    # Return the JSON response
    return jsonify(result)

@application.route("/locations/<int:id>")
def get_location(id):
    # Fetch a specific location by ID from the database
    location = Location.query.get(id)

    # Check if the location exists
    if not location:
        abort(404, description="Location not found")

    # Serialize the data using the location_schema
    result = location_schema.dump(location)

    # Return the JSON response
    return jsonify(result)

@application.route("/transport", methods=["GET"])
def transport():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    sort_map = {
        "name": Transport.name.asc(),
        "rating": Transport.rating.desc(),
        "city": Transport.city.asc(),
        "zip": Transport.zip.asc(),
    }

    if sort :
        order_by_clause = sort_map[sort]

    if search:
        if sort:
            query = db.session.query(Transport).filter(
                or_(
                    Transport.city.ilike(f"%{search}%"),
                    Transport.zip.ilike(f"%{search}%"),
                    Transport.name.ilike(f"%{search}%"),
                    Transport.types.ilike(f"%{search}%"),
                    Transport.reviews.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            query = db.session.query(Transport).filter(
                or_(
                    Transport.city.ilike(f"%{search}%"),
                    Transport.zip.ilike(f"%{search}%"),
                    Transport.name.ilike(f"%{search}%"),
                    Transport.types.ilike(f"%{search}%"),
                    Transport.reviews.ilike(f"%{search}%")
                )
            )
    else:
        if sort:
            query = db.session.query(Transport).order_by(order_by_clause)
        else:
            query = db.session.query(Transport)

    # Serialize the data using the transport_schema
    result = transport_schema.dump(query, many=True)

    # Return the JSON response
    return jsonify(result)

@application.route("/transport/<int:id>")
def get_transport_option(id):
    # Fetch specific transportation data by ID from the database
    transport = Transport.query.get(id)

    # Check if the transportation data exists
    if not transport:
        abort(404, description="Transportation data not found")

    # Serialize the data using the transport_schema
    result = transport_schema.dump(transport)

    # Return the JSON response
    return jsonify(result)

if __name__ == "__main__":
    application.run(host="0.0.0.0", port=8080)
