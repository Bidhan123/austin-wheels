import React from 'react'
import axios from 'axios'

const getBranchNames = async () => {
    try {
        const response = await axios.get('https://gitlab.com/api/v4/projects/43437643/repository/branches');
        const branches = response.data.map((branch) => branch.name);
        return branches;
    } catch (error) {
        console.error(error);
    }    
}

export default GitLab