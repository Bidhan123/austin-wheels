# Website Link
- [austin-wheels.me](https://www.austin-wheels.me/)
- [api.austin-wheels.me](https://api.austin-wheels.me/)

# API documentation
[Postman Documentation](https://documenter.getpostman.com/view/29992579/2s9YJZ3Po1)

# Gitlab Pipelines
https://gitlab.com/sraina11/austin-wheels/-/pipelines

# cs373 Group 19
| Name                    | GitLabID     | EID     |
| ----------------------- | ------------ | ------- |
| Saransh Raina           | @sraina11    | sr52767 |
| Ethan Lopez             | @ethanslopez | esl834  |
| Fernando Salas          | @frsalas513  | frs445  |
| Minh-Triet (Silas) Pham | @mtnp        | mnp923  |
| Bidhan Devkota          | @bidhan123   | bd25229 |

## Git SHA

| Phase | Git Sha                                  |
| ----- | ---------------------------------------- |
| 1     | 37e7389b2dc15bf9c7f1ea27bb262ddab211922a |
| 2     | 1014f067f46be2d925cb838d129c40af59243298 |
| 3     | 790e5184dfd7e7304bc4db3ae64d1ce448b143a3 |
| 4     | ab3906dbc090ad37ca125b56cb430d4d0ebfc942 |

## Phase Leaders

| Phase | Project Leader          |
| ----- | ----------------        |
| 1     | Saransh Raina           |
| 2     | Ethan Lopez             |
| 3     | Minh-Triet Pham (Silas) |
| 4     | Fernando Salas          |

# Completion Times
## Phase 1

| Name                    | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ----------------------- | -------------------------- | -------------------------- |
| Saransh Rain            | 8                          | 7                          |
| Ethan Lopez             | 8                          | 9                          | 
| Fernando Salas          | 7                          | 7                          | 
| Minh-Triet (Silas) Pham | 7                          | 8                          | 
| Bidhan Devkota          | 9                          | 8                          |

## Phase 2

| Name                    | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ----------------------- | -------------------------- | -------------------------- |
| Saransh Rain            | 24                         | 28                         |
| Ethan Lopez             | 24                         | 26                         | 
| Fernando Salas          | 24                         | 28                         | 
| Minh-Triet (Silas) Pham | 24                         | 25                         | 
| Bidhan Devkota          | 24                         | 25                         |

## Phase 3

| Name                    | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ----------------------- | -------------------------- | -------------------------- |
| Saransh Rain            | 14                         | 14                         |
| Ethan Lopez             | 15                         | 14                         | 
| Fernando Salas          | 12                         | 13                         | 
| Minh-Triet (Silas) Pham | 13                         | 13                         | 
| Bidhan Devkota          | 12                         | 13                         |

## Phase 4

| Name                    | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ----------------------- | -------------------------- | -------------------------- |
| Saransh Rain            | 7                          | 8                          |
| Ethan Lopez             | 6                          | 8                          | 
| Fernando Salas          | 7                          | 8                          | 
| Minh-Triet (Silas) Pham | 6                          | 8                          | 
| Bidhan Devkota          | 6                          | 6                          |

Comments: 
- About page heavily inspired and originally written by [Gallery Guide](https://gitlab.com/swe-spring-2023/art-project/) project from Spring 2023 semester.
- Backend Code heavily inspired and originally written by [NutriNet](https://gitlab.com/goesoscar/NutriNet/) project from Spring 2023 semester.

## Austin Wheels

## Proposal
Austin Wheels is a project to serve the wheelchair bound community in the Austin Area. Our site will provide information about wheelchair accessible locations and transporation options in the Greater Austin Area.

## Data Source URLs
- Google Places API: https://developers.google.com/maps/documentation/places/web-service
- Transit API: https://transitapp.com/apis
- Open Street Map WheelMap API: https://wheelmap.org/search

## The 3 Models
- Zip Codes in Region (109)
- Wheelchair accessible locations (3400)
- Wheelchair friendly transporation stops (592)

## Describe 5 attributes per model that can be filtered or sorted 
- Zip Codes in Region
    - City
    - County 
    - Latitude
    - Longitude
    - Number of Wheelchair accessible locations
    - Number of Wheelchair friendly transportation stops

- Wheelchair accessible locations
    - Zip Code located in
    - City located in
    - Type of location (Resturant, Education, Healthcare, etc)
    - Rating
    - Days/Hours Open

- Wheelchair friendly transporation stops
    - Zip Code located in
    - City located in
    - Rating
    - Days/Hours Open
    - Type of transport (Bus, Train, Car, etc)

## Connections between Instances of the Models
- Zip Code
    - Can display and connect to wheelchair accesible locations and transport stops within Zip Code
- Wheelchair accessible locations
    - Can display Zip Code located in as well as nearby wheelchair friendly transportation stops
- Wheelchair friendly transporation stops
    - Can display Zip Code located in as well as nearby wheelchair accessible locations

## Types of media for Instances of the Models
- Zip Code
    - Map with Zip Code territory labeled
    - Image of area
- Wheelchair accessible locations
    - Images of the location
    - User reviews of the location
    - Icon of location type
- Wheelchair friendly transporation stops
    - Images of the stop
    - Map of the stop's location
    - User reviews of the stop
    - Icon for type of stop

## Questions our Site will answer
- Where can wheelchair bound individuals in Greater Austin Area go to eat/get healthcare/be entertined?
- What transportation stops and options are wheelchair friendly in the area?
- What Zip Codes in the Greater Austin Area are abundant in wheelchair friendly options?
